Nama Anggota kelompok:

1. Abdillah Evan Nurdhiawan - 1906398805
2. Yadhit Prasetya - 1906353725
3. Nur Hafsari Setyorini - 1906353706
4. Kukuh Satrio Wicaksono - 1906399045
5. Muhammad Hasbi - 1906398811

Link Herokuapp :
https://studbuddy.herokuapp.com/

Pada kali ini kelompok kami ingin membuat sebuah website yang dapat digunakan oleh para pelajar baik pelajar dari SMP-Kuliah selama masa pandemi ini. Sebagai rincian lebih lengkapnya lagi adalah website kami dapat membantu para pelajar untuk mencari teman-teman sesama pelajarnya yang dapat saling meluangkan waktu untuk melakukan kegiatan akademis seperti belajar bersama. Selain itu, website ini juga menyediakan koleksi lagu-lagu Lo-Fi yang dengan harapan dapat membantu para pelajar untuk tetap fokus pada kegiatan pembelajaran mereka. 
Website kami memiliki beberapa fitur interaktif yang dapat memudahkan para pengguna untuk menggunakan website ini. Diantaranya adalah:
- Fitur untuk registrasi pengguna baru, kami memiliki kecenderungan untuk membuat fitur ini untuk kalangan warga UI terlebih dahulu dengan menggunakan akun UI yang telah diberikan oleh UI
- Fitur taruh jadwal, pada fitur ini para pengguna dapat meletakkan jadwal belajar mereka, sebagai benchmark kami memakai seperti fitur pada aplikasi ClassUp
- Fitur find classmates, untuk fitur ini mungkin hampir mirip dengan penggunaan media sosial pada umumnya para pengguna dapat mencari dan menambahkan orang lain sebagai mates mereka pada website kami
- Fitur Resources Gathering, pada fitur ini para pengguna dapat mengirimkan materi apa saja seperti catatan perkuliahan,ppt,ebook ataupun yang lainnya berupa link. Para pengguna lain juga dapat melihat materi tersebut, jadi seperti saling bertukar informasi/catatan terhadap suatu pelajaran.
- Fitur Recommended Study Song, bagi para pengguna website kami dapat menambahkan playlist kesukaan mereka di website kami dan dapat membagikannya untuk para teman mereka di website kami.
